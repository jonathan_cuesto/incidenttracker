﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using IncidentTracker.Models;

namespace IncidentTracker.Controllers
{
    public class incidentsController : ApiController
    {
        private IncidentTrackerEntities db = new IncidentTrackerEntities();

        // GET: api/incidents
        public IQueryable<incident> Getincidents()
        {
            return db.incidents.Where(x => x.isArchived == false);
        }

        // GET: api/incidents/5
        [ResponseType(typeof(incident))]
        public IHttpActionResult Getincident(string id)
        {
            incident incident = db.incidents.Find(id);
            if (incident == null)
            {
                return NotFound();
            }

            return Ok(incident);
        }

        // POST: api/incidents
        [ResponseType(typeof(incident))]
        public IHttpActionResult Postincident(incident incident)
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            string kind = GetKind(incident);

            if (kind == null)
            {
                return BadRequest();
            }

            locality location = GetLocation(incident);

            if (location == null)
            {
                return BadRequest();
            }

            db.incidents.Add(incident);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (incidentExists(incident.C_id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = incident.C_id }, incident);
        }

        private locality GetLocation(incident incident)
        {
            return db.localities.Find(incident.locationId);
        }

        private static string GetKind(incident incident)
        {
            String[] kindArray = new String[] { "ROBBERY", "MURDER", "TRAFFIC_ACCIDENT", "SHOOTING", "ASSAULT" };
            var kind = kindArray.Where(x => x == incident.kind).SingleOrDefault();
            return kind;
        }

        [Route("api/incidents/{id}/archive")]
        [HttpPost]
        [ResponseType(typeof(incident))]
        public IHttpActionResult Archiveincident(string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            incident incident = db.incidents.Find(id);
            if (incident == null)
            {
                return NotFound();
            }

            incident.isArchived = true;

            db.Entry(incident).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!incidentExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(incident);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool incidentExists(string id)
        {
            return db.incidents.Count(e => e.C_id == id) > 0;
        }
    }
}