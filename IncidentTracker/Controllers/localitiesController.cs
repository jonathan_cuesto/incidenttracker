﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using IncidentTracker.Models;

namespace IncidentTracker.Controllers
{
    public class localitiesController : ApiController
    {
        private IncidentTrackerEntities db = new IncidentTrackerEntities();

        // GET: api/localities
        public IQueryable<locality> Getlocalities()
        {
            return db.localities;
        }

        // GET: api/localities/5
        [ResponseType(typeof(locality))]
        public IHttpActionResult Getlocality(string id)
        {
            locality locality = db.localities.Find(id);
            if (locality == null)
            {
                return NotFound();
            }

            return Ok(locality);
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool localityExists(string id)
        {
            return db.localities.Count(e => e.C_id == id) > 0;
        }
    }
}